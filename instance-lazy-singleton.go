package goit

import "sync"

type InstanceLazySingleton[T any] struct {
	sync.Once
	name     string
	instance T
	builder  Builder[T]
}

func newInstanceLazySingleton[T any](name string, builder Builder[T]) Instance[T] {
	return &InstanceLazySingleton[T]{
		name:    name,
		builder: builder,
	}
}

func (this *InstanceLazySingleton[T]) getName() string {
	return this.name
}

func (this *InstanceLazySingleton[T]) getInstance(injector *Injector) (instance T, err error) {
	this.Do(func() {
		err = this.builtin(injector)
	})
	if err != nil {
		return empty[T](), err
	}
	return this.instance, nil
}

func (this *InstanceLazySingleton[T]) builtin(injector *Injector) (err error) {
	defer func() {
		if recovery := recover(); recovery != nil {
			if e, ok := recovery.(error); ok {
				err = e
			} else {
				panic(recovery)
			}
		}
	}()

	instance, err := this.builder(injector)

	if err != nil {
		return
	}

	this.instance = instance
	return nil
}
