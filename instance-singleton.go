package goit

type InstanceSingleton[T any] struct {
	name     string
	instance T
}

func newInstanceSingleton[T any](name string, instance T) Instance[T] {
	return &InstanceSingleton[T]{
		name:     name,
		instance: instance,
	}
}

func (this *InstanceSingleton[T]) getName() string {
	return this.name
}

func (this *InstanceSingleton[T]) getInstance(injector *Injector) (T, error) {
	return this.instance, nil
}
