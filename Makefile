#test:
#	go test -race -v ./...

lint:
	golangci-lint run --timeout 60s --max-same-issues 50 ./...

lint-fix:
	golangci-lint run --timeout 60s --max-same-issues 50 --fix ./...
	

tidy:
	go mod tidy

# make tag VERSION=v1.0.0
tag:
	git tag ${VERSION}
	git push origin ${VERSION}

# make publish VERSION=v1.0.0
publish: tidy tag
