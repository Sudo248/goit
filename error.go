package goit

import "fmt"

func instanceNotFound(name string) error {
	return fmt.Errorf("goit: not found instance with name %s", name)
}
