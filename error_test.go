package goit

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInstanceNotFound(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	err := instanceNotFound("test")

	assertion.NotNil(err)
	assertion.Equal("goit: not found instance with name test", err.Error())
}
