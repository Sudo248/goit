package goit

type InstanceFactory[T any] struct {
	name    string
	factory Builder[T]
}

func newInstanceFactory[T any](name string, factory Builder[T]) Instance[T] {
	return &InstanceFactory[T]{
		name:    name,
		factory: factory,
	}
}

func (this *InstanceFactory[T]) getName() string {
	return this.name
}

func (this *InstanceFactory[T]) getInstance(injector *Injector) (instance T, err error) {
	defer func() {
		if recovery := recover(); recovery != nil {
			if e, ok := recovery.(error); ok {
				err = e
			} else {
				panic(recovery)
			}
		}
	}()

	_instance, err := this.factory(injector)

	if err != nil {
		return empty[T](), err
	}

	return _instance, nil
}
