# 💉 goit - Dependency injection for Go

**💪🏻 A dependency injection toolkit based on Go Generics.**

**⚡ Require go version 1.22.1+**
## 💡 Features

- Instance registration
- Instance get
- Instance require
- Named or anonymous instance
- Lazy singleton, singleton value or factory
- Default injector
- Lightweight, no dependencies
- No code generation

## 🚀 Install

```sh
go get gitlab.com/Sudo248/goit
```

**✴️For more information goto [goit](https://pkg.go.dev/gitlab.com/Sudo248/goit)✴️**

## ✅Happy coding✅