package goit

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInstanceSingletonName(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	type test struct {
		foobar string
	}
	_test := test{foobar: "foobar"}

	provider1 := func(i *Injector) (int, error) {
		return 42, nil
	}
	provider2 := func(i *Injector) (test, error) {
		return _test, nil
	}

	instance1 := newInstanceSingleton("foobar", provider1)
	assertion.Equal("foobar", instance1.getName())

	instance2 := newInstanceSingleton("foobar", provider2)
	assertion.Equal("foobar", instance2.getName())
}

func TestInstanceSingleton(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	type test struct {
		foobar string
	}
	_test := test{foobar: "foobar"}

	service1 := newInstanceSingleton("foobar", _test)
	assertion.Equal(&InstanceSingleton[test]{name: "foobar", instance: _test}, service1)

	instance1, err1 := service1.getInstance(nil)
	assertion.Nil(err1)
	assertion.Equal(_test, instance1)

	service2 := newInstanceSingleton("foobar", 42)
	assertion.Equal(&InstanceSingleton[int]{name: "foobar", instance: 42}, service2)

	instance2, err2 := service2.getInstance(nil)
	assertion.Nil(err2)
	assertion.Equal(42, instance2)
}
