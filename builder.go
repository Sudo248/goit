package goit

type Builder[T any] func(*Injector) (T, error)
