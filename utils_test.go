package goit

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEmptyInstance(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	value1 := empty[int]()
	assertion.Empty(value1)

	value2 := empty[*int]()
	assertion.Nil(value2)
	assertion.Empty(value2)
}

func TestGenerateInstanceName(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	type test struct{} //nolint:unused

	type interfaceTest interface{}

	name := getDefaultInstanceName[test]()
	assertion.Equal("goit.test", name)

	name = getDefaultInstanceName[*test]()
	assertion.Equal("*goit.test", name)

	name = getDefaultInstanceName[int]()
	assertion.Equal("int", name)

	name = getDefaultInstanceName[*interfaceTest]()
	assertion.Equal("*goit.interfaceTest", name)

	name = getDefaultInstanceName[interfaceTest]()
	assertion.Equal("*goit.interfaceTest", name)
}
