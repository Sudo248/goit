package goit

import "sync"

type Injector struct {
	sync.RWMutex
	instances map[string]any
}

var DefaultInjector = New()

func New() *Injector {
	return &Injector{
		RWMutex:   sync.RWMutex{},
		instances: make(map[string]any),
	}
}

func getInjectorOrDefault(injector *Injector) *Injector {
	if injector != nil {
		return injector
	}

	return DefaultInjector
}

func (this *Injector) exists(name string) bool {
	this.RLock()
	defer this.RUnlock()

	_, existed := this.instances[name]

	return existed
}

func (this *Injector) get(name string) (any, bool) {
	this.RLock()
	defer this.RUnlock()

	instance, existed := this.instances[name]

	return instance, existed
}

func (this *Injector) set(name string, instance any) {
	this.Lock()
	defer this.Unlock()

	this.instances[name] = instance
}

func (this *Injector) remove(name string) {
	this.Lock()
	defer this.Unlock()

	delete(this.instances, name)
}
