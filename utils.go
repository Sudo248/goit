package goit

import "fmt"

func empty[T any]() (t T) {
	return
}

func getDefaultInstanceName[T any]() string {
	var t T

	// get name for struct
	name := fmt.Sprintf("%T", t)

	if name != "<nil>" {
		return name
	}

	return fmt.Sprintf("%T", new(T))
}
