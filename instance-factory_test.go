package goit

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInstanceFactoryName(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	type test struct {
		foobar string
	}
	_test := test{foobar: "foobar"}

	provider1 := func(i *Injector) (int, error) {
		return 42, nil
	}
	provider2 := func(i *Injector) (test, error) {
		return _test, nil
	}

	instance1 := newInstanceFactory("foobar", provider1)
	assertion.Equal("foobar", instance1.getName())

	instance2 := newInstanceFactory("foobar", provider2)
	assertion.Equal("foobar", instance2.getName())
}

func TestInstanceFactory(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	type test struct {
		foobar string
	}
	_test := test{foobar: "foobar"}

	provider1 := func(i *Injector) (int, error) {
		return 42, nil
	}
	provider2 := func(i *Injector) (test, error) {
		return _test, nil
	}
	provider3 := func(i *Injector) (int, error) {
		panic("error")
	}
	provider4 := func(i *Injector) (int, error) {
		panic(fmt.Errorf("error"))
	}
	provider5 := func(i *Injector) (int, error) {
		return 42, fmt.Errorf("error")
	}

	i := New()

	service1 := newInstanceFactory("foobar", provider1)
	instance1, err1 := service1.getInstance(i)
	assertion.Nil(err1)
	assertion.Equal(42, instance1)

	service2 := newInstanceFactory("hello", provider2)
	instance2, err2 := service2.getInstance(i)
	assertion.Nil(err2)
	assertion.Equal(_test, instance2)

	assertion.Panics(func() {
		service3 := newInstanceFactory("baz", provider3)
		_, _ = service3.getInstance(i)
	})

	assertion.NotPanics(func() {
		service4 := newInstanceFactory("plop", provider4)
		instance4, err4 := service4.getInstance(i)
		assertion.NotNil(err4)
		assertion.Empty(instance4)
		expected := fmt.Errorf("error")
		assertion.Equal(expected, err4)
	})

	assertion.NotPanics(func() {
		service5 := newInstanceFactory("plop", provider5)
		instance5, err5 := service5.getInstance(i)
		assertion.NotNil(err5)
		assertion.Empty(instance5)
		expected := fmt.Errorf("error")
		assertion.Equal(expected, err5)
	})
}
