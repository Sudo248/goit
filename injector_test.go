package goit

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInjectorNew(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	i := New()
	assertion.NotNil(i)
	assertion.Empty(i.instances)
}

func TestInjectorListRegisterSingleton(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	i := New()

	assertion.NotPanics(func() {
		RegisterSingletonWithInjector[int](i, 42)
		RegisterSingletonWithInjector[float64](i, 21)
	})

	assertion.Len(i.instances, 2)
}
