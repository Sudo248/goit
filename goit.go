package goit

import (
	"log"
)

// =========================================REGISTER==========================================================

// ===== LazySingleton =====

func RegisterLazySingleton[T any](builder Builder[T]) {
	RegisterLazySingletonWithInjector(DefaultInjector, builder)
}

func RegisterNamedLazySingleton[T any](name string, builder Builder[T]) {
	RegisterNamedLazySingletonWithInjector(DefaultInjector, name, builder)
}

func RegisterLazySingletonWithInjector[T any](injector *Injector, builder Builder[T]) {
	name := getDefaultInstanceName[T]()
	RegisterNamedLazySingletonWithInjector[T](injector, name, builder)
}

func RegisterNamedLazySingletonWithInjector[T any](injector *Injector, name string, builder Builder[T]) {
	_injector := getInjectorOrDefault(injector)

	if _injector.exists(name) {
		log.Printf("[WARNING] `%s` already registered", name)
		return
	}

	instance := newInstanceLazySingleton(name, builder)

	_injector.set(name, instance)
}

// ===== Singleton =====

func RegisterSingleton[T any](instance T) {
	RegisterSingletonWithInjector(DefaultInjector, instance)
}

func RegisterNamedSingleton[T any](name string, instance T) {
	RegisterNamedSingletonWithInjector(DefaultInjector, name, instance)
}

func RegisterSingletonWithInjector[T any](injector *Injector, instance T) {
	name := getDefaultInstanceName[T]()
	RegisterNamedSingletonWithInjector(injector, name, instance)
}

func RegisterNamedSingletonWithInjector[T any](injector *Injector, name string, instance T) {
	_injector := getInjectorOrDefault(injector)
	if _injector.exists(name) {
		log.Printf("[WARNING] `%s` already registered", name)
		return
	}

	_instance := newInstanceSingleton(name, instance)

	_injector.set(name, _instance)
}

// ===== Factory =====

func RegisterFactory[T any](factory Builder[T]) {
	RegisterFactoryWithInjector[T](DefaultInjector, factory)
}

func RegisterNamedFactory[T any](name string, factory Builder[T]) {
	RegisterNamedFactoryWithInjector[T](DefaultInjector, name, factory)
}

func RegisterFactoryWithInjector[T any](injector *Injector, factory Builder[T]) {
	name := getDefaultInstanceName[T]()
	RegisterNamedFactoryWithInjector[T](injector, name, factory)
}

func RegisterNamedFactoryWithInjector[T any](injector *Injector, name string, factory Builder[T]) {
	_injector := getInjectorOrDefault(injector)

	if _injector.exists(name) {
		log.Printf("[WARNING] `%s` already registered", name)
		return
	}

	instance := newInstanceFactory(name, factory)

	_injector.set(name, instance)
}

// =========================================GET==========================================================

func Get[T any]() (T, error) {
	return GetWithInjector[T](DefaultInjector)
}

func GetNamed[T any](name string) (T, error) {
	return GetNamedWithInjector[T](DefaultInjector, name)
}

func GetWithInjector[T any](injector *Injector) (T, error) {
	name := getDefaultInstanceName[T]()
	return GetNamedWithInjector[T](injector, name)
}

func GetNamedWithInjector[T any](injector *Injector, name string) (T, error) {
	_injector := getInjectorOrDefault(injector)

	instanceAny, ok := _injector.get(name)

	if !ok {
		return empty[T](), instanceNotFound(name)
	}

	instance, ok := instanceAny.(Instance[T])

	if !ok {
		return empty[T](), instanceNotFound(name)
	}

	_instance, err := instance.getInstance(_injector)

	if err != nil {
		return empty[T](), err
	}

	return _instance, nil
}

// =========================================REQUIRE==========================================================

func Require[T any]() T {
	instance, err := GetWithInjector[T](DefaultInjector)
	if err != nil {
		panic(err)
	}
	return instance
}

func RequireNamed[T any](name string) T {
	instance, err := GetNamedWithInjector[T](DefaultInjector, name)
	if err != nil {
		panic(err)
	}
	return instance
}

func RequireWitInjector[T any](injector *Injector) T {
	instance, err := GetWithInjector[T](injector)
	if err != nil {
		panic(err)
	}
	return instance
}

func RequireNamedWitInjector[T any](injector *Injector, name string) T {
	instance, err := GetNamedWithInjector[T](injector, name)
	if err != nil {
		panic(err)
	}
	return instance
}
