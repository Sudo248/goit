package goit

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBuilder(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	var builder Builder[int] = func(injector *Injector) (int, error) {
		return 100, nil
	}

	data, err := builder(DefaultInjector)

	assertion.Equal(data, 100)
	assertion.Nil(err)

	var builder1 Builder[int] = func(injector *Injector) (int, error) {
		panic(fmt.Errorf("Panic"))
	}

	assertion.Panics(func() {
		builder1(DefaultInjector)
	})
}
