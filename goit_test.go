package goit

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDefaultInjector(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	type test struct {
		foobar string
	}

	RegisterLazySingleton(func(injector *Injector) (*test, error) {
		return &test{foobar: "foobar"}, nil
	})

	assertion.Len(DefaultInjector.instances, 1)

	instance, err := Get[*test]()

	assertion.Equal(test{foobar: "foobar"}, *instance)
	assertion.Nil(err)
}

func TestLazySingleTon(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	type test struct {
		foobar string
	}

	RegisterLazySingleton(func(injector *Injector) (*test, error) {
		return &test{foobar: "foobar"}, nil
	})

	assertion.Len(DefaultInjector.instances, 1)

	instance, err := Get[*test]()

	assertion.Equal(test{foobar: "foobar"}, *instance)
	assertion.Nil(err)

	instance.foobar = "Duong"

	_instance, err := Get[*test]()

	assertion.Equal(_instance.foobar, "Duong")
	assertion.Equal(instance.foobar, "Duong")
	assertion.Equal(instance.foobar, _instance.foobar)
	assertion.Nil(err)
}

func TestSingleTon(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	type test struct {
		foobar string
	}

	RegisterSingleton(&test{foobar: "foobar"})

	assertion.Len(DefaultInjector.instances, 1)

	instance, err := Get[*test]()

	assertion.Equal(test{foobar: "foobar"}, *instance)
	assertion.Nil(err)

	instance.foobar = "Duong"

	_instance, err := Get[*test]()

	assertion.Equal(_instance.foobar, "Duong")
	assertion.Equal(instance.foobar, "Duong")
	assertion.Equal(instance.foobar, _instance.foobar)
	assertion.Nil(err)
}

func TestFactory(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	type test struct {
		foobar string
	}

	counter := 0
	RegisterFactory(func(injector *Injector) (test, error) {
		fmt.Printf("Factory: %d \n", counter)
		counter += 1
		return test{foobar: "foobar"}, nil
	})

	assertion.Len(DefaultInjector.instances, 1)

	instance, err := Get[test]()
	otherInstance, err := Get[test]()

	assertion.Equal(instance.foobar, otherInstance.foobar)
	assertion.NotEqual(fmt.Sprintf("%p", &instance), fmt.Sprintf("%p", &otherInstance))
	assertion.Nil(err)
}
