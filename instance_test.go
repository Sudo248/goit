package goit

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

type testInstance[T any] struct {
	name     string
	instance T
}

func (this *testInstance[T]) getName() string {
	return this.name
}

func (this *testInstance[T]) getInstance(injector *Injector) (T, error) {
	return this.instance, nil
}

func TestImplementInstance(t *testing.T) {
	assertion := assert.New(t)
	DefaultInjector = New()
	var instance Instance[int] = &testInstance[int]{
		name:     "test",
		instance: 100,
	}

	assertion.Equal(instance.getName(), "test")
	assertion.Equal(fmt.Sprintf("%T", instance.(*testInstance[int])), "*goit.testInstance[int]")
}
