package goit

type Instance[T any] interface {
	getName() string
	getInstance(injector *Injector) (T, error)
}
